package annotation;
/*
 sample test class to test cornlp server, the lib folder contains all the required jars. 
 ADD ALL THE JARS TO THE CLASSPATH
 stanfordcorenlp,
 stanford-corenlp-full-2015-12-09
stanford-postagger-2015-12-09
jcommander-1.58.jar
 author:amita
 */


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;


public class testSimpleCoreNLP_3_5_2 {
	protected SimpleCoreNLP_3_5_2options opts = new SimpleCoreNLP_3_5_2options();
	protected JCommander jc = null;
	
	
	/**
	 * Run the program based on the command-line options in {@link FacetSimilarityBootstrapOptions}.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void go(String[] args) throws IOException, InterruptedException {
		jc = new JCommander(opts);
		try {
			jc.parse(args);
		} catch (ParameterException e) {
			System.out.printf("Error processing command line arguments: %s\n", 
					e.getMessage());
			jc.usage();
			System.exit(-1);
		}
		
		String posmodel=opts.getposmodel();
		SimpleCoreNLP_3_5_2 nlpobj =new SimpleCoreNLP_3_5_2(posmodel);

		String test1 = "Author1 :1 - The Magna Carta dates back to 1215 .\n" +
				"Fast forward to 1689 - '' Heller explored the right s origins , noting that the 1689 English Bill of Rights explicitly protected a right to keep arms for self-defense , 554 U. S. , at ___ ___ -LRB- slip op. , at 19 20 -RRB- , and that by 1765 , Blackstone was able to assert that the right to keep and bear arms was ' one of the fundamental rights of Englishmen , ' id. , at ___ -LRB- slip op. , at 20 -RRB- . ''\n" +
				"-LRB- McDonald v. Chicago -RRB- Well , the English Historians Brief in the McDonald case says that Heller was wrong about that .\n" +
				"A brief is '' a written argument submitted to a court '' for those uninformed about legal matters .\n" +
				"I have had many people on your side tell me that the Second Amendment protects a preexisting right .\n" +
				"But in its preexisting form , the RKBA was connected with militia service .\n" +
				"So pointing out that the RKBA is a preexisting right does not support your argument at all .\n" +
				"Author2 :1 - Then prove it already !\n" +
				"Show us exactly where it has been established that any person who was physically incapable of taking part in the militia , either due to age , or gender , or physical injury , was barred from ever owning a firearm for any reason .\n" +
				"After all according to you , and people like Heinrich Himmler , there 's no purpose in regular people owning arms if they ca n't serve the state .\n" +
				"Show us where and end the matter once and for all .\n" +
				"Author1 :2 - Why would ownership of weapons by those outside the militia prove that such ownership is protected by the RKBA ?\n" +
				"Author2 :2 - Because you 've argued over , and over , and over , and over again , that the right to keep and bear arms existed for the sole and exclusive purpose of guaranteeing that the militia , and only the militia , may be armed in case they 're needed to be called upon for defense of the country .\n" +
				"You 've come out and argued that the right to keep and bear arms was never considered to be applied for any other purpose , such as defense of the individuals against those that would threaten them with harm .\n" +
				"In other words your argument is that those who are female , or under 18 , or over 46 , who were no longer physically able to serve in the militia , were barred from ever possessing a firearm , because their status of being armed was n't in the interest of the government .\n" +
				"That 's your argument , the long and short of it .\n" +
				"Now prove once and for all that this was a cold hard fact of history , or drop the argument once and for all .\n" +
				"Author1 :3 - A lack of a ban on firearms ownership by those outside of the militia does not necessarily mean that such ownership was considered to be protected by the RKBA .\n" +
				"That 's a non-sequitir .\n" +
				"Author2 :3 - Yes it does .\n" +
				"Being unable to serve in the militia meant that you could no longer fulfill a duty or obligation to the state because you were obsolete .\n" +
				"If the Second Amendment were purely about the militia , and absolutely nothing else , then there would be no way retired members would be allowed to remain armed for their own private purposes .\n" +
				"You 're not going to slip away from this one and pretend you were never confronted .\n" +
				"You made the statement , now you have to prove it .\n" +
				"No it is n't .\n" +
				"You 've claimed many times that the Second Amendment had absolutely nothing to do with an individual 's personal defense .\n" +
				"And now that you 've been challenged to prove it you 're trying to run away from it .\n" +
				"Well it 's not going to work .\n" +
				"You 're obligated to prove beyond reasonable doubt that your argument is the correct one .\n" +
				"You 're charged with proving that those who were either women or over 46 were n't allowed to be armed , because them being armed was n't in the interest of the state due to the fact that they could n't serve in the militia .\n" +
				"Get to work !\n" +
				"Author1 :4 - You 're asking me to prove a claim I 've never made .\n" +
				"I never argued there was a complete ban on firearms used for non-militia purposes .\n" +
				"Saul Cornell never argued that .\n" +
				"His argument was that weapons not constitutionally protected were subject to reasonable regulation by the state , not that there was a complete ban on such weapons - '' The only weapons singled out for constitutional protection were those connected to militia service ... Any law , including firearms regulations , had to be aimed at a legitimate public purpose and had to be consistent with reason . ''\n" +
				"Saul Cornell | Forum : Comment : Early American Gun Regulation and the Second Amendment : A Closer Look at the Evidence | Law and History Review , 25.1 | The History Cooperative .";
		//String test1="Notice not one of the common sense gun control laws gun hater pilot is demanding is to punish dangerous criminals by keeping them in prison to keep guns away from them!";
		ArrayList<HashMap<String, LinkedList<Object>>> hmaplist1 =nlpobj.parsed_text(test1);
		 for (HashMap<String, LinkedList<Object>> hmap1 : hmaplist1)
		 {
			 for (Entry<String, LinkedList<Object>> entry : hmap1.entrySet()) {
			    String key = entry.getKey();
			    Object value = entry.getValue();
			    System.out.print(key);
			    System.out.println(value);
			}
			 
		 }
		System.out.println();
		System.out.println();
		System.out.println();
		ArrayList<HashMap<String, LinkedList<Object>>> hmaplist2 =nlpobj.parse_text_with_coref(test1, 0);
		for (HashMap<String, LinkedList<Object>> hmap1 : hmaplist2)
		{
			for (Entry<String, LinkedList<Object>> entry : hmap1.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				System.out.print(key);
				System.out.println(value);
			}

		}
		System.out.println();
		System.out.println();
		System.out.println();
		ArrayList<HashMap<String, LinkedList<Object>>> hmaplist3 =nlpobj.parse_text_with_coref(test1, 0);
		for (HashMap<String, LinkedList<Object>> hmap1 : hmaplist3)
		{
			for (Entry<String, LinkedList<Object>> entry : hmap1.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				System.out.print(key);
				System.out.println(value);
			}

		}
	}


	public static void main(String[] args)throws Exception  {
		    new testSimpleCoreNLP_3_5_2().go(args);
		    
	}

}

/*
 * Expected output
 * deps_cc[[amod, 2, 1], [nsubj, 3, 2], [dep, 13, 3], [xcomp, 3, 4], [mark, 6, 5], [xcomp, 4, 6], [dobj, 6, 7], [case, 11, 8], [amod, 11, 9], [compound, 11, 10], [nmod:to, 6, 11], [nsubj, 13, 12], [punct, 13, 14], [root, 0, 13]]
pos[JJS, NNS, VBP, VB, TO, VB, DT, TO, JJ, NNS, NN, PRP, VBP, .]
lemmas[most, christian, dont, try, to, do, that, to, gay, people, either.sure, they, do, .]
tokens[Most, Christians, dont, try, to, do, that, to, gay, people, either.Sure, they, do, .]
parse[(ROOT (S (S (NP (JJS Most) (NNS Christians)) (VP (VBP dont) (VP (VB try) (S (VP (TO to) (VP (VB do) (NP (DT that)) (PP (TO to) (NP (JJ gay) (NNS people) (NN either.Sure))))))))) (NP (PRP they)) (VP (VBP do)) (. .)))]
char_offset[[0, 4], [5, 15], [16, 20], [21, 24], [25, 27], [28, 30], [31, 35], [36, 38], [39, 42], [43, 49], [50, 61], [62, 66], [67, 69], [69, 70]]
deps_cc[[root, 0, 1]]
pos[.]
lemmas[.]
tokens[.]
parse[(ROOT (NP (. .)))]
char_offset[[71, 72]]

 */
 
