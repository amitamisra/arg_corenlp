/*
 sample test class to test cornlp server, the lib folder contains all the required jars. 
 ADD ALL THE JARS TO THE CLASSPATH
 stanfordcorenlp,
 stanford-corenlp-full-2015-12-09
stanford-postagger-2015-12-09
jcommander-1.58.jar
 author:amita
 */
package annotation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations;
import edu.stanford.nlp.dcoref.CorefChain;
public class AtestSimpleCoreNLP_3_5_2 {
	protected SimpleCoreNLP_3_5_2options opts = new SimpleCoreNLP_3_5_2options();
	protected JCommander jc = null;
	
	
	/**
	 * Run the program based on the command-line options in {@link FacetSimilarityBootstrapOptions}.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void go(String[] args) throws IOException, InterruptedException {
		jc = new JCommander(opts);
		try {
			jc.parse(args);
		} catch (ParameterException e) {
			System.out.printf("Error processing command line arguments: %s\n", 
					e.getMessage());
			jc.usage();
			System.exit(-1);
		}
		
		String posmodel=opts.getposmodel();
		SimpleCoreNLP_3_5_2 nlpobj =new SimpleCoreNLP_3_5_2(posmodel);
		
		String test1 = "Most Christians dont try to do that to gay people either.Sure they do. .";
		//String test1="Notice not one of the common sense gun control laws gun hater pilot is demanding is to punish dangerous criminals by keeping them in prison to keep guns away from them!"; 
		ArrayList<HashMap<String, LinkedList<Object>>> hmaplist1 =nlpobj.parsed_text(test1);
		 for (HashMap<String, LinkedList<Object>> hmap1 : hmaplist1)
		 {
			 for (Entry<String, LinkedList<Object>> entry : hmap1.entrySet()) {
			    String key = entry.getKey();
			    Object value = entry.getValue();
			    System.out.print(key);
			    System.out.println(value);
			}
			 
		 }
	}
	public static void main(String[] args)throws Exception  {
		    new AtestSimpleCoreNLP_3_5_2().go(args);
		    
	}

}

/*
 * Expected output
 * deps_cc[[amod, 2, 1], [nsubj, 3, 2], [dep, 13, 3], [xcomp, 3, 4], [mark, 6, 5], [xcomp, 4, 6], [dobj, 6, 7], [case, 11, 8], [amod, 11, 9], [compound, 11, 10], [nmod:to, 6, 11], [nsubj, 13, 12], [punct, 13, 14], [root, 0, 13]]
pos[JJS, NNS, VBP, VB, TO, VB, DT, TO, JJ, NNS, NN, PRP, VBP, .]
lemmas[most, christian, dont, try, to, do, that, to, gay, people, either.sure, they, do, .]
tokens[Most, Christians, dont, try, to, do, that, to, gay, people, either.Sure, they, do, .]
parse[(ROOT (S (S (NP (JJS Most) (NNS Christians)) (VP (VBP dont) (VP (VB try) (S (VP (TO to) (VP (VB do) (NP (DT that)) (PP (TO to) (NP (JJ gay) (NNS people) (NN either.Sure))))))))) (NP (PRP they)) (VP (VBP do)) (. .)))]
char_offset[[0, 4], [5, 15], [16, 20], [21, 24], [25, 27], [28, 30], [31, 35], [36, 38], [39, 42], [43, 49], [50, 61], [62, 66], [67, 69], [69, 70]]
deps_cc[[root, 0, 1]]
pos[.]
lemmas[.]
tokens[.]
parse[(ROOT (NP (. .)))]
char_offset[[71, 72]]

 */
 
