package annotation;
import com.sun.org.apache.xalan.internal.xsltc.compiler.SyntaxTreeNode;

import edu.stanford.nlp.hcoref.CorefCoreAnnotations;
import edu.stanford.nlp.hcoref.data.CorefChain;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.*;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;
//import org.jetbrains.annotations.NotNull;
import py4j.GatewayServer;

import java.util.*;


public class SimpleCoreNLP_3_5_2{
	private StanfordCoreNLP pipeline;
	
	public SimpleCoreNLP_3_5_2(String posmodel_loc)
	{
		Properties props = new Properties();
	    props.put("pos.model", posmodel_loc);
        props.put("annotators", "tokenize, ssplit, pos, lemma,ner, parse,sentiment, mention, coref");
	    this.pipeline = new StanfordCoreNLP(props);

	}
	public StanfordCoreNLP getpipeline()
	{
		return this.pipeline;
	}
	
	
	public LinkedList<Object> getdeplist(SemanticGraph dependencies )
	{    LinkedList<Object> all_dep_lists =new LinkedList<Object>();
		List <SemanticGraphEdge> edge_set1 = dependencies.edgeListSorted();
		for(SemanticGraphEdge edge : edge_set1)
	      {    ArrayList<Object> deplist= new ArrayList<Object>();
	          IndexedWord dep = edge.getDependent();
	          IndexedWord gov = edge.getGovernor();
	      GrammaticalRelation relation = edge.getRelation();
	      deplist.add( relation.toString());
	      deplist.add(gov.index());
	      deplist.add(dep.index());
	      all_dep_lists.add(deplist);
		}
		
		 ArrayList<Object> deplist= new ArrayList<Object>();
	      IndexedWord root= dependencies.getFirstRoot();
	      int rootindex= root.index();
	      String relation = new String("root");
	      deplist.add( relation.toString());
	      deplist.add(0);
	      deplist.add(rootindex);
	    
	      all_dep_lists.add(deplist);
	      return  all_dep_lists;
	}

	public ArrayList<HashMap<String,LinkedList<Object>>> parsed_text(String text)
	{
		Annotation annotation = this.pipeline.process(text);
        List<CoreMap> sentences = annotation.get(SentencesAnnotation.class);

        ArrayList<HashMap <String,LinkedList<Object>>> hm_list =  new ArrayList <HashMap<String,LinkedList<Object>>>();
		LinkedList<Object> corefList = new LinkedList<>();

		for (CorefChain cc : annotation.get(CorefCoreAnnotations.CorefChainAnnotation.class).values()) {
			corefList.add(cc.toString());
		}
         
        for(CoreMap sentence: sentences) {
		    int numNers = 0, numAdj =0, numAdv =0;
            List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);
            HashMap <String,LinkedList<Object>> hm =  new HashMap <String,LinkedList<Object>>();
             LinkedList<Object> tokenList = new LinkedList<Object>();
         	 LinkedList<Object> lemmaList = new LinkedList<Object>();
         	 LinkedList<Object> posList = new LinkedList<Object>();
         	 LinkedList<Object> parsetreeList = new LinkedList<Object>();
         	 LinkedList<Object> charoffsetList = new LinkedList<Object>();
			 LinkedList<Object> corefMention =  new LinkedList<Object>();
             LinkedList<Object> sentimentList =  new LinkedList<Object>();
             LinkedList<Object> nerList =  new LinkedList<Object>();
             LinkedList<Object> numNerList =  new LinkedList<Object>();
             LinkedList<Object> numAdjList =  new LinkedList<Object>();
             LinkedList<Object> numAdvList =  new LinkedList<Object>();


            LinkedList<Object> newSentence =  new LinkedList<Object>();
            newSentence.add(sentence.toString());

            Tree tree = sentence.get(SentimentCoreAnnotations.SentimentAnnotatedTree.class);
            int sentiment = RNNCoreAnnotations.getPredictedClass(tree);

            sentimentList.add(sentiment);

           // sentence.get(SyntaxTreeNode.class)
         	 
             for (CoreLabel token: tokens) { 
            	   ArrayList<Object>  offsetList= new ArrayList<Object>();
                   String word = token.get(TextAnnotation.class);
                   Integer index= token.get(IndexAnnotation.class);
                   String pos=token.getString(PartOfSpeechAnnotation.class);
                   String lemma=token.getString(LemmaAnnotation.class);
                   String ner = token.getString(NamedEntityTagAnnotation.class);
                   //System.out.println(token.toString() + " NER " + ner + " POS " + pos);
                   Integer offsetbegin= token.get(CharacterOffsetBeginAnnotation.class);
                   Integer offsetend=token.get(CharacterOffsetEndAnnotation.class);
                   offsetList.add(offsetbegin);
                   offsetList.add(offsetend);
                   charoffsetList.add(offsetList);
                   tokenList.add(word);
                   lemmaList.add(lemma);
                   posList.add(pos);
                   nerList.add(ner);
                   if(!ner.startsWith("O")) numNers++;
                   if(pos.startsWith("JJ"))numAdj++;
                   if(pos.startsWith("RB"))numAdv++;
             }
             numNerList.add(numNers);
             numAdjList.add(numAdj);
             numAdvList.add(numAdv);

             tree = sentence.get(TreeAnnotation.class);

             parsetreeList.add(tree.toString());

             SemanticGraph depgraph = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);

             hm.put("tokens",  tokenList);
             hm.put("pos",posList);
             hm.put("lemmas", lemmaList);
             hm.put("char_offset", charoffsetList);
             hm.put("parse", parsetreeList);
             hm.put("deps_cc",getdeplist(depgraph));
             hm.put("coref", corefList);
             hm.put("coref mention", corefMention);
             hm.put("sentiment", sentimentList);
             hm.put("ner", nerList);
             hm.put("num_adj", numAdjList);
             hm.put("num_adv", numAdvList);
             hm.put("num_ner", numNerList);
             hm.put("new_sentence", newSentence);

             hm_list.add(hm);
             
         }
         
        
        
         return hm_list;
		
	}
	public ArrayList<HashMap<String,LinkedList<Object>>>  parse_text_with_coref(String text, int replaceInSentence)
    {
        Annotation annotation = this.pipeline.process(text);
        text = replaceWithCoref(annotation, replaceInSentence);
        return  parsed_text(text);
    }
	public static void main(String[] args) {
	    // app is now the gateway.entry_point
        String pos_model="lib/stanford-postagger-2015-12-09/models/english-bidirectional-distsim.tagger";
        GatewayServer server = new GatewayServer(new SimpleCoreNLP_3_5_2(pos_model),25340);
	    server.start();
	    System.out.println("Gateway Server Started");
	  }
    public List<String> pos_tagger(String text) {
        String pos_model="lib/stanford-postagger-2015-12-09/models/english-bidirectional-distsim.tagger";
        Properties props = new Properties();
        props.setProperty("tagSeparator", "/");
        MaxentTagger tagger = new MaxentTagger(
                pos_model, props);

        ArrayList<String> output = new ArrayList<>();
        for(String sentence : text.split("\\."))
        {
            output.add(tagger.tagString(sentence));
        }
        return output;

    }

   // @NotNull
    //replaceInSentence 1: true , 0: false - intelligent replacement, replace only first occurence in every sentence not containing the
    //representative mention
    private  String replaceWithCoref( Annotation doc, int replaceInSentence){

        Map<Integer, CorefChain> corefChainMap = doc.get(CorefCoreAnnotations.CorefChainAnnotation.class);
        List<CoreMap> sentences = doc.get(CoreAnnotations.SentencesAnnotation.class);
        List<String> resolvedStrings = new ArrayList<>();
        int lastSentIndex = -1;
        for (CoreMap sentence : sentences) {

            List<CoreLabel> tokens = sentence.get(CoreAnnotations.TokensAnnotation.class);
            for (CoreLabel token : tokens) {

                Integer corefClustId= token.get(CorefCoreAnnotations.CorefClusterIdAnnotation.class);
                CorefChain chain = corefChainMap.get(corefClustId);
                if(chain==null || chain.getMentionsInTextualOrder().size() == 1){
                    resolvedStrings.add(token.word());
                }else{

                    int sentIndex = chain.getRepresentativeMention().sentNum -1;
                    CoreMap corefSentence = sentences.get(sentIndex);
                    List<CoreLabel> corefSentenceTokens = corefSentence.get(CoreAnnotations.TokensAnnotation.class);
                    CorefChain.CorefMention reprMent = chain.getRepresentativeMention();
                    if (( replaceInSentence == 0 && sentIndex == token.sentIndex())  || (replaceInSentence == 0 && lastSentIndex == token.sentIndex()) ||
                            (replaceInSentence ==1 && sentIndex == token.sentIndex() &&  reprMent.startIndex <= token.index() && reprMent.endIndex > token.index()))
                        resolvedStrings.add(token.word());
                    else{
                        for(int i = reprMent.startIndex; i<reprMent.endIndex; i++){
                            CoreLabel matchedLabel = corefSentenceTokens.get(i-1);
                            String input  = matchedLabel.word();
                            String formatted_input  = input.replaceAll("[^a-zA-Z\\s]", "").replaceAll("\\s+", " ");
                            resolvedStrings.add(formatted_input);

                        }
                        lastSentIndex =  token.sentIndex();
                    }
                }
            }
        }

        StringBuilder stringBuilder =new StringBuilder("");
        for (String str : resolvedStrings) {
            stringBuilder.append(str + " ");
        }
       return  stringBuilder.toString();

    }
}


